package com.example.cardemo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleRestController {
	@GetMapping("/test/")
	public String test() {
		return "This endpoint is accessible";
	}

	@GetMapping("/healthcheck/")
	public String healthcheck(){
		return "This endpoint is running 2.0";
	}
}
