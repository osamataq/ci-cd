#!/bin/bash
sleep 10
#!/bin/bash
mkdir tests
curl -o tests/test1.json http://192.168.237.84:8079/healthcheck/
if cat tests/test1.json | grep -q "This endpoint is running 2.0"
then
    curl -o tests/test2.json http://192.168.237.84:8079/test/
    if cat tests/test2.json | grep -q "This endpoint is accessible"
    then 
        exit 0
    else 
        exit 1
    fi
else 
    exit 1
fi
