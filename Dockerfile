FROM openjdk:8-jdk-alpine   
ARG JAR_FILE=*.jar   
COPY target/${JAR_FILE} /test/appc.jar   
WORKDIR /test   
ENTRYPOINT ["java","-jar","/test/appc.jar"]   
RUN apk --no-cache add bash   
RUN apk --no-cache add curl  
CMD bash  